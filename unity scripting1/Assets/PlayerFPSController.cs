using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPSController : MonoBehaviour
{

    //variables
    public GameObject camerasParent;
    public float walkSpeed = 5f;  //Walk Speed
    public float hRotationSpeed = 100f; // Player rotates along y axis 
    public float vRotationSpeed = 80f; //Cam rotates along x axis


    // Start is called before the first frame update
    void Start()
    {
        //hide and lock mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);
    }

    // Update is called once per frame
    private void movement()
    {
        //codigo de movimiento
        {
            //movement
            float hMovement = Input.GetAxisRaw("Horizontal");
            float vMovement = Input.GetAxisRaw("Vertical");

            Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
            transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));


            //rotation
            float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
            float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

            transform.Rotate(0f, hPlayerRotation, 0f);
            camerasParent.transform.Rotate(-vCamRotation, 0f, 0f);

        }





    }
}

